import { combineReducers } from "redux";
import { shoppingCartReducer } from "./shoppingCartReducer";

export const rootShoppingReducer = combineReducers({
    shoppingCartReducer: shoppingCartReducer,
})