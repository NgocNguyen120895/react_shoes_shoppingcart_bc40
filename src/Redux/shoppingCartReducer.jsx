
const stateCart = {
    cart: []
}


export const shoppingCartReducer = (state = stateCart, action) => {
    switch (action.type) {
        case "ADD_TO_CART": {
            let clonedCart = [...state.cart];
            let index = clonedCart.findIndex(item => {
                return item.id === action.payload.id
            })
            if (index !== -1) {
                clonedCart[index].qty += 1
            } else {
                clonedCart.push(action.payload)
            }
            state.cart = clonedCart;
            return { ...state };
        }
        case "XOA_ITEM": {
            let clonedCart = [...state.cart];
            clonedCart.splice(action.index, 1);
            state.cart = clonedCart;
            return { ...state }
        }
        case "CHANGE_QTY": {
            let clonedCart = [...state.cart];
            if (action.ToF) {
                clonedCart[action.id].qty += 1
            } else {
                if (clonedCart[action.id].qty > 1) {
                    clonedCart[action.id].qty -= 1;
                }
            }
            state.cart = clonedCart;
            return { ...state }
        }
        case "ON_PURCHASED": {
            let clonedCart = [...state.cart];
            if (clonedCart.length !== 0) {
                clonedCart.splice(0, clonedCart.length)
                state.cart = clonedCart;
                alert("Thank you for shopping with us, see you next time!");
            } else {
                alert("Your cart is empty, please add at least 1 product");
            }
            return { ...state }
        }
    }
    return state
}