import React, { Component } from 'react'
import CartModalRedux from './CartModalRedux'
import ProductListRedux from './ProductListRedux'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTruckFast } from '@fortawesome/free-solid-svg-icons'



class ShoppingCartReduxEx extends Component {
    render() {

        const element = <FontAwesomeIcon icon={faTruckFast} />

        let soLuongSP = this.props.theCart.reduce((tongSL, item, index) => {
            return tongSL += item.qty;
        }, 0)

        return (
            <div className="container py-5">
                <button className='btn btn-primary' data-toggle="modal" data-target="#exampleModal">
                    {element} ({soLuongSP})
                </button>
                <CartModalRedux></CartModalRedux>
                <ProductListRedux></ProductListRedux>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        theCart: state.shoppingCartReducer.cart,
    }
}

export default connect(mapStateToProps)(ShoppingCartReduxEx) 