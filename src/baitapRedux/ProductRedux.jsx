import React, { Component } from 'react'
import { connect } from 'react-redux';

class ProductRedux extends Component {

    render() {
        const { id, name, price, image, shortDescription } = this.props.sanPham;
        return (
            <div>
                <div className="card text-white bg-dark" style={{ height: 450 }} >
                    <img className="card-img-top" src={image} alt={image} width={200} height={200}/>
                    <div className="card-body">
                        <h5 className="card-title">{id} - {name}</h5>
                        <p className="card-text">{shortDescription}</p>
                        <p>PRICE: {price}</p>
                        <button onClick={() => { this.props.addToCartHandle(this.props.sanPham) }} className='btn btn-danger mb-5'>
                            ADD TO CART</button>
                    </div>
                </div>

            </div >
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addToCartHandle: (product) => {
            const newProduct = {
                id: product.id,
                name: product.name,
                alias: product.alias,
                price: product.price,
                description: product.description,
                shortDescription: product.shortDescription,
                qty: 1,
                image: product.image,
            };
            const action = {
                type: "ADD_TO_CART",
                payload: newProduct,
            }
            dispatch(action)
        },
     
    }
}
export default connect(null, mapDispatchToProps)(ProductRedux)