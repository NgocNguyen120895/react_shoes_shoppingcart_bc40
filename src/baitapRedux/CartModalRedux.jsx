import React, { Component } from 'react'
import { connect } from 'react-redux'

class CartModalRedux extends Component {


    renderProduct = () => {
        return this.props.cartToMap.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td><button className='btn btn-warning mx-2' style={{ width: 40 }} onClick={() => { this.props.quantityHandle(index, false) }}>-</button>
                        {item.qty}
                        <button className='btn btn-warning mx-2' style={{ width: 40 }} onClick={() => { this.props.quantityHandle(index, true) }}>+</button></td>
                    <td><img src={item.image} alt={item.image} width={40} height={40} /></td>
                    <td>
                        {item.price * item.qty}
                    </td>
                    <td><button className='btn btn-danger' onClick={() => { this.props.delItemByIndex(index) }}>Xoa</button></td>
                </tr >
            )
        })
    }

    render() {
        return (
            <div className='container'>

                <div className="modal fade" id="exampleModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Your Shopping Cart</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className='table'>
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>PRODUCT NAME</th>
                                            <th>PRICE</th>
                                            <th>QUANTITY</th>
                                            <th>IMAGE</th>
                                            <th>Sub-total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.renderProduct()}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colSpan="6">TOTAL: </td>
                                            <td>
                                                {this.props.cartToMap.reduce((tongTien, item, index) => { return tongTien += item.qty * item.price }, 0)}
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary text-white" data-dismiss="modal" onClick={this.props.onPurchase}>PURCHASE NOW!</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartToMap: state.shoppingCartReducer.cart,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        delItemByIndex: (index) => {
            const action = {
                type: "XOA_ITEM",
                index,
            };
            dispatch(action);
        },
        quantityHandle: (id, ToF) => {
            const action = {
                type: "CHANGE_QTY",
                id, ToF,
            };
            dispatch(action);
        },
        onPurchase: () => {
            const action = { type: "ON_PURCHASED" };
            dispatch(action);
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CartModalRedux) 