import React, { Component } from 'react'
import Shoes_Data from "../shopping_cart/DataProduct"
import ProductRedux from './ProductRedux'



export default class ProductListRedux extends Component {

    renderProductList = () => {
        return Shoes_Data.map((product, index) => {
            return (
                <div key={index} className="col-3 mb-3">
                    <ProductRedux sanPham={product}></ProductRedux>
                </div>
            )
        })
    }

    render() {
        return (
            <div className='container'>
                <h3>DANH SACH SAN PHAM</h3>
                <div className="row">
                    {this.renderProductList()}
                </div>
            </div>
        )
    }
}
