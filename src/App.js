import './App.css';
import ShoppingCartReduxEx from './baitapRedux/ShoppingCartReduxEx';
// import ProductEx from './shopping_cart/ProductEx';


function App() {
  return (
    <div className="App">
      {/* <ProductEx></ProductEx> */}
      <ShoppingCartReduxEx></ShoppingCartReduxEx>
    </div>
  );
}

export default App;
