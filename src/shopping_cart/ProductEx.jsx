import React, { Component } from 'react'
import ProductList from './ProductList'
import ProductModal from './ProductModal'
import ShoesData from "./DataProduct";


export default class ProductEx extends Component {
    state = {
        CART: []
    }

    addToCart = (addedProduct) => {
        let cloneCart = [...this.state.CART]
        let index = cloneCart.findIndex((item) => {
            return item.id === addedProduct.id
        })
        if (index === -1) {
            let product = { ...addedProduct, soLuong: 1 }
            cloneCart.push(product)
        } else {
            cloneCart[index].soLuong++
        }
        this.setState({
            CART: cloneCart,
        })
    }

    removeItemHandle = (id) => {
        console.log("Test")
        let clonedCart = [...this.state.CART]
        let index = clonedCart.findIndex(item => item.id === id)
        if (index !== -1) {
            clonedCart.splice(index, 1)
        }
        this.setState({
            CART: clonedCart,
        })
    }

    quantityHandler = (id, qty) => {
        let clonedCart = [...this.state.CART];
        let index = clonedCart.findIndex(item => item.id === id)

        if (qty) {
            clonedCart[index].soLuong++;
        } else {
            if (clonedCart[index].soLuong > 1) {
                clonedCart[index].soLuong -= 1;
            }
        }
        this.setState({
            CART: clonedCart,
        })
    }


    render() {
        let totalProducts = this.state.CART.reduce((tsl, item, index) => {
            return tsl += item.soLuong
        }, 0)


        return (
            <div>
                <div className="container">
                    <ProductModal cart={this.state.CART} onclickRemove={this.removeItemHandle} quantityHandler={this.quantityHandler}></ProductModal>
                    <i className="fa-light fa-cart-shopping" />

                    <div className='text-right'>
                        <span className='text-danger bold' style={{ cursor: "pointer" }} data-toggle="modal" data-target="#modelId">
                            ({totalProducts})</span>
                    </div>
                    <ProductList productArray={ShoesData} addToCart={this.addToCart}></ProductList>
                </div>
            </div>
        )
    }
}
