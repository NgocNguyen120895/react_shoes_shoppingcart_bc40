import React, { Component } from 'react'

export default class Products extends Component {
    render() {

        const { product, addToCart } = this.props;

        return (
            <div>
                <div className="card text-primary">
                    <img className="card-img-top" src={product.image} alt={product.image} width={200} height={200} />
                    <div className="card-body bg-dark">
                        <p className="card-text">{product.name}</p>
                        <p className="card-text">{product.price}</p>
                        <button className='btn btn-danger' onClick={() => { addToCart(product) }}>Add to cart</button>
                    </div>
                </div>
            </div>
        )
    }
}
