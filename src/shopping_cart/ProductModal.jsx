import React, { Component } from 'react'

export default class ProductModal extends Component {

    renderCart = () => {
        return this.props.cart.map((item, index) => {
            return (
                <tr key={index} className="px-5">
                    <td>{item.id}</td>
                    <td> <img src={item.image} alt={item.image} width={100}></img></td>
                    <td>{item.name}</td>
                    <td>
                        <button className='btn btn-warning' onClick={() => { this.props.quantityHandler(item.id, false) }}>-</button>
                        {item.soLuong}
                        <button className='btn btn-warning' onClick={() => { this.props.quantityHandler(item.id, true) }}>+</button>
                    </td>
                    <td>{item.price}</td>
                    <td>{item.soLuong * item.price}</td>
                    <td><button className='btn btn-danger' onClick={() => { this.props.onclickRemove(item.id) }}>X</button></td>
                </tr>
            )
        })
    }

    render() {
        return (

            <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div class="modal-dialog .modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header d-block">
                            <h5 class="modal-title text-center">CART</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body d-flex">
                            <table className='container'>
                                <thead>
                                    <tr>
                                        <th>Product id</th>
                                        <th>Image</th>
                                        <th>Product Name</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Total</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderCart()}
                                </tbody>
                                <tfoot>
                                    
                                    {this.props.cart.reduce((gTotal, cart, index) => { return gTotal += cart.price * cart.soLuong }, 0)}
                                </tfoot>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
