import React, { Component } from 'react'
import Products from './Products'

export default class ProductList extends Component {

    renderProduct = () => {
        return this.props.productArray.map((product, index) => {
            return (
                <div className='col-3 p-3'>
                    <div key={index}>
                        <Products product={product} addToCart={this.props.addToCart}></Products>
                    </div>
                </div>
            )
        })
    }

    render() {
        return (
            <div className='row'>
                {this.renderProduct()}
            </div>
        )
    }
}
